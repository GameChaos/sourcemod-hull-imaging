
#define PLUGIN_NAME           "Raytracing test"
#define PLUGIN_AUTHOR         "GameChaos"
#define PLUGIN_DESCRIPTION    ""
#define PLUGIN_VERSION        "1.00"
#define PLUGIN_URL            ""

#define MAX_PIC_WIDTH				3840
#define MAX_PIC_HEIGHT				2160
#define ASPECT_RATIO				1.7777777777777
#define FOV							106.26020470831195740628877488181	// vertical fov

#define MINS						view_as<float>({ -16.0, -16.0, 0.0 })
#define MAXS						view_as<float>({ 16.0, 16.0, 72.0 })

#define RAYTRACE_POSITION_USAGE		"Usage: sm_raytrace_position <x> <y> <z> <pitch> <yaw> <roll> <trace_type: 0|1; 0 = ray;1 = hull> <filename> [mask_flags; default: \"MASK_PLAYERSOLID\"]"
#define RAYTRACE_USAGE				"Usage: sm_raytrace <trace_type: 0|1; 0 = ray;1 = hull> <filename> [mask_flags; default: \"MASK_PLAYERSOLID\"]"

#include <sourcemod>
#include <sdktools>
#include <gamechaos>

#pragma semicolon 1
#pragma dynamic 62144

enum
{
	TRACETYPE_RAY,
	TRACETYPE_HULL,
	TRACETYPE_COUNT
}

ConVar g_cvRaytraceFov;
ConVar g_cvRaytracePicWidth;
ConVar g_cvRaytracePicHeight;

StringMap g_smMaskFlags;
StringMap g_smTraceType;

// rgb colours: [r, g, b] * n
int g_iPicture[MAX_PIC_WIDTH * MAX_PIC_HEIGHT * 3];

float g_fAspectRatio;

public Plugin myinfo =
{
	name = PLUGIN_NAME,
	author = PLUGIN_AUTHOR,
	description = PLUGIN_DESCRIPTION,
	version = PLUGIN_VERSION,
	url = PLUGIN_URL
};

public void OnPluginStart()
{
	RegAdminCmd("sm_raytrace_position", Command_SmRaytracePosition, ADMFLAG_CHEATS, RAYTRACE_POSITION_USAGE);
	RegAdminCmd("sm_raytrace", Command_SmRaytrace, ADMFLAG_CHEATS, "Traces an image of the collision hull of the world from the player's perspective.");
	
	g_cvRaytraceFov			= CreateConVar("sm_raytrace_fov", "90.0", _, _, true, 0.01, true, 180.0);
	g_cvRaytracePicWidth	= CreateConVar("sm_raytrace_pic_width", "1920", _, _, true, 1.0, true, 3840.0);
	g_cvRaytracePicHeight	= CreateConVar("sm_raytrace_pic_height", "1080", _, _, true, 1.0, true, 2160.0);
	
	g_fAspectRatio = 1920.0 / 1080.0 / (4 / 3);
	
	HookConVarChange(g_cvRaytraceFov, OnConVarChanged);
	
	AutoExecConfig(.name = "gcraytracing");
	
	g_smMaskFlags = new StringMap();
	g_smMaskFlags.SetValue("MASK_ALL", MASK_ALL);
	g_smMaskFlags.SetValue("MASK_SOLID", MASK_SOLID);
	g_smMaskFlags.SetValue("MASK_PLAYERSOLID", MASK_PLAYERSOLID);
	g_smMaskFlags.SetValue("MASK_NPCSOLID", MASK_NPCSOLID);
	g_smMaskFlags.SetValue("MASK_WATER", MASK_WATER);
	g_smMaskFlags.SetValue("MASK_OPAQUE", MASK_OPAQUE);
	g_smMaskFlags.SetValue("MASK_OPAQUE_AND_NPCS", MASK_OPAQUE_AND_NPCS);
	g_smMaskFlags.SetValue("MASK_VISIBLE", MASK_VISIBLE);
	g_smMaskFlags.SetValue("MASK_VISIBLE_AND_NPCS", MASK_VISIBLE_AND_NPCS);
	g_smMaskFlags.SetValue("MASK_SHOT", MASK_SHOT);
	g_smMaskFlags.SetValue("MASK_SHOT_HULL", MASK_SHOT_HULL);
	g_smMaskFlags.SetValue("MASK_SHOT_PORTAL", MASK_SHOT_PORTAL);
	g_smMaskFlags.SetValue("MASK_SOLID_BRUSHONLY", MASK_SOLID_BRUSHONLY);
	g_smMaskFlags.SetValue("MASK_PLAYERSOLID_BRUSHONLY", MASK_PLAYERSOLID_BRUSHONLY);
	g_smMaskFlags.SetValue("MASK_NPCSOLID_BRUSHONLY", MASK_NPCSOLID_BRUSHONLY);
	g_smMaskFlags.SetValue("MASK_NPCWORLDSTATIC", MASK_NPCWORLDSTATIC);
	g_smMaskFlags.SetValue("MASK_SPLITAREAPORTAL", MASK_SPLITAREAPORTAL);
	
	g_smTraceType = new StringMap();
	g_smTraceType.SetValue("TRACETYPE_RAY", TRACETYPE_RAY);
	g_smTraceType.SetValue("TRACETYPE_HULL", TRACETYPE_HULL);
	g_smTraceType.SetValue("ray", TRACETYPE_RAY);
	g_smTraceType.SetValue("hull", TRACETYPE_HULL);
}

public void OnConVarChanged(ConVar convar, const char[] oldValue, const char[] newValue)
{
	if (convar == g_cvRaytracePicWidth
		|| convar == g_cvRaytracePicHeight)
	{
		convar.FloatValue = float(convar.IntValue);
		
		g_fAspectRatio = g_cvRaytracePicWidth.FloatValue / g_cvRaytracePicHeight.FloatValue / (4 / 3);
	}
}

public Action Command_SmRaytracePosition(int client, int args)
{
	int cmdArgs = GetCmdArgs();
	
	if (cmdArgs != 8 && cmdArgs != 9)
	{
		ReplyToCommand(client, "Invalid usage. %s", RAYTRACE_POSITION_USAGE);
		return Plugin_Handled;
	}
	
	char buffer[32];
	float position[3];
	float angles[3];
	//GetCmdArg(1, buffer, sizeof(buffer));
	for (int i; i < 3; i++)
	{
		GetCmdArg(i + 1, buffer, sizeof(buffer));
		position[i] = StringToFloat(buffer);
		GetCmdArg(i + 4, buffer, sizeof(buffer));
		angles[i] = StringToFloat(buffer);
	}
	
	GetCmdArg(7, buffer, sizeof(buffer));
	int traceType;
	if (!g_smTraceType.GetValue(buffer, traceType))
	{
		traceType = StringToInt(buffer);
	}
	
	char fileName[PLATFORM_MAX_PATH];
	GetCmdArg(8, fileName, sizeof(fileName));
	
	int maskFlags = MASK_PLAYERSOLID;
	if (cmdArgs > 8)
	{
		GetCmdArg(9, buffer, sizeof(buffer));
		
		if (!g_smMaskFlags.GetValue(buffer, maskFlags))
		{
			maskFlags = StringToInt(buffer);
		}
	}
	
	TraceRayImage(position, angles, traceType, maskFlags, fileName);
	
	return Plugin_Handled;
}

public Action Command_SmRaytrace(int client, int args)
{
	int cmdArgs = GetCmdArgs();
	
	if (cmdArgs != 2 && cmdArgs != 3)
	{
		ReplyToCommand(client, "Invalid usage. %s", RAYTRACE_USAGE, cmdArgs);
		return Plugin_Handled;
	}
	
	float position[3];
	float angles[3];
	GetClientEyePosition(client, position);
	GetClientEyeAngles(client, angles);
	
	int traceType;
	char buffer[32];
	GetCmdArg(1, buffer, sizeof(buffer));
	if (!g_smTraceType.GetValue(buffer, traceType))
	{
		traceType = StringToInt(buffer);
	}
	
	char fileName[PLATFORM_MAX_PATH];
	GetCmdArg(2, fileName, sizeof(fileName));
	
	int maskFlags = MASK_PLAYERSOLID;
	if (cmdArgs > 2)
	{
		GetCmdArg(3, buffer, sizeof(buffer));
		
		if (!g_smMaskFlags.GetValue(buffer, maskFlags))
		{
			maskFlags = StringToInt(buffer);
		}
	}
	
	TraceRayImage(position, angles, traceType, maskFlags, fileName);
	
	return Plugin_Handled;
}

void TraceRayImage(const float position[3], const float angles[3], int traceType, int maskFlags, char[] fileName)
{
	PrintToServer("Pos { %f, %f, %f } Ang { %f, %f, %f }", position[0], position[1], position[2], angles[0], angles[1], angles[2]);
	
	int iPixel;
	
	float invWidth = 1.0 / g_cvRaytracePicWidth.FloatValue;
	float invHeight = 1.0 / g_cvRaytracePicHeight.FloatValue;
	
	// angle diff
	float fov = ConvertFov(g_cvRaytraceFov.FloatValue);
	
	float angle = Tangent(3.1415926535 * 0.5 * fov / 180.0);
	
	// y is for yaw
	for (int y; y < g_cvRaytracePicHeight.FloatValue; y++)
	{
		//fAngles[1] = fStartAngles[1];
		// x is for pitch
		for (int x; x < g_cvRaytracePicWidth.FloatValue; x++)
		{
			float xx = (2.0 * (x * invWidth) - 1.0) * angle;
			float yy = (1.0 - 2.0 * (y * invHeight)) * angle / g_fAspectRatio;
			
			float pos2[3];
			pos2[0] = xx;
			pos2[1] = yy;
			pos2[2] = -1.0;
			
			//NormalizeVector(pos2, pos2);
			
			// x axis aka pitch
			RotateVector(pos2, view_as<float>({ 1.0, 0.0, 0.0 }), DegToRad(90.0 - angles[0]), pos2);
			
			// z axis aka yaw
			RotateVector(pos2, view_as<float>({ 0.0, 0.0, 1.0 }), DegToRad(angles[1] - 90.0), pos2);
			ScaleVector(pos2, 10000.0);
			AddVectors(position, pos2, pos2);
			
			switch (traceType)
			{
				case TRACETYPE_HULL:
					TraceHullPixel(position, pos2, iPixel, maskFlags);
				default:
					TraceRayPixel(position, pos2, iPixel, maskFlags);
			}
			//fAngles[1] -= fAngStep;
		}
		//fAngles[0] += fAngStep;
	}
	
	PrintToServer("Tracing done.");
	GenerateImage(fileName);
}

// v: a vector in 3D space
// k: a unit vector describing the axis of rotation
// theta: the angle (in radians) that v rotates around k
void RotateVector(const float v[3], const float k[3], float theta, float result[3])
{
	/*std::cout << "Rotating " << glm::to_string(v) << " "
	<< theta << " radians around "
	<< glm::to_string(k) << "..." << std::endl;*/
	
	float cos_theta = Cosine(theta);
	float sin_theta = Sine(theta);
	
	//glm::dvec3 rotated = (v * cos_theta) + (glm::cross(k, v) * sin_theta) + (k * glm::dot(k, v)) * (1 - cos_theta);
	
	float cross[3];
	GetVectorCrossProduct(k, v, cross);
	float dot = GetVectorDotProduct(k, v);
	
	for (int i; i < 3; i++)
	{
		result[i] = (v[i] * cos_theta) + (cross[i] * sin_theta) + (k[i] * dot) * (1.0 - cos_theta);
	}
	
	//std::cout << "Rotated: " << glm::to_string(rotated) << std::endl;
}

float ConvertFov(float fovDegrees)
{
	return RadToDeg(2.0 * ArcTangent(Tangent(DegToRad(fovDegrees) / 2.0) * (4.0 / 3.0)));
}

void TraceRayPixel(const float position[3], const float pos2[3], int &pixel, int maskFlags)
{
	// changed to raytype_endpoint
	
	TR_TraceRayFilter(position, pos2, maskFlags, RayType_EndPoint, GCTraceEntityFilterPlayer);
	if (!TR_DidHit())
	{
		g_iPicture[pixel++] = 0;
		g_iPicture[pixel++] = 0;
		g_iPicture[pixel++] = 0;
		return;
	}
	float fEndPos[3];
	TR_GetEndPosition(fEndPos);
	float fNormal[3];
	TR_GetPlaneNormal(INVALID_HANDLE, fNormal);
	//int iDistance = RoundFloat(Sigmoid(GetVectorDistance(position, fEndPos)) * 127.0);
	
	// colour
	GenerateColours(pixel, fNormal, fEndPos);
}

void TraceHullPixel(const float position[3], const float pos2[3], int &pixel, int maskFlags)
{
	TR_TraceHullFilter(position, pos2, MINS, MAXS, maskFlags, GCTraceEntityFilterPlayer);
	if (!TR_DidHit())
	{
		g_iPicture[pixel++] = 0;
		g_iPicture[pixel++] = 0;
		g_iPicture[pixel++] = 0;
		return;
	}
	float fEndPos[3];
	TR_GetEndPosition(fEndPos);
	float fNormal[3];
	TR_GetPlaneNormal(INVALID_HANDLE, fNormal);
	//int iDistance = RoundFloat(Sigmoid(GetVectorDistance(position, fEndPos)) * 127.0);
	
	GenerateColours(pixel, fNormal, fEndPos);
}

void GenerateColours(int &pixel, const float normal[3], const float endPos[3])
{
	// colour
	g_iPicture[pixel++] = RoundFloat((normal[0] + 1.0) / 2.0 * 255.0);
	g_iPicture[pixel++] = RoundFloat((normal[1] + 1.0) / 2.0 * 255.0);
	g_iPicture[pixel++] = RoundFloat((normal[2] + 1.0) / 2.0 * 255.0); //255;
	
	// grid
	for (int i; i < 3; i++)
	{
		if (RoundToFloor(endPos[i]) % 16 == 0)
		{
			int reduction = RoundToFloor(32.0 * (1.0 - FloatAbs(normal[i])));
			g_iPicture[pixel - 3] -= reduction;
			g_iPicture[pixel - 2] -= reduction;
			g_iPicture[pixel - 1] -= reduction;
		}
		
		if (RoundToFloor(endPos[i] * 16.0) % 16 == 0)
		{
			int reduction = RoundToFloor(16.0 * (1.0 - FloatAbs(normal[i])));
			g_iPicture[pixel - 3] -= reduction;
			g_iPicture[pixel - 2] -= reduction;
			g_iPicture[pixel - 1] -= reduction;
		}
	}
	
	// clamp colours
	for (int i = 1; i <= 3; i++)
	{
		if (g_iPicture[pixel - i] < 0)
		{
			g_iPicture[pixel - i] = 0;
		}
		else if (g_iPicture[pixel - i] > 255)
		{
			g_iPicture[pixel - i] = 255;
		}
	}
}

void GenerateImage(char[] fileName)
{
	int tga[18];
	
	tga[2] = 2;
	tga[12] = 255 & g_cvRaytracePicWidth.IntValue;
	tga[13] = 255 & (g_cvRaytracePicWidth.IntValue >> 8);
	tga[14] = 255 & g_cvRaytracePicHeight.IntValue;
	tga[15] = 255 & (g_cvRaytracePicHeight.IntValue >> 8);
	tga[16] = 24;
	tga[17] = 32;
	
	char path[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, path, sizeof(path), "raytrace/");
	if (!DirExists(path))
	{
		CreateDirectory(path, 511);
	}
	
	Format(path, sizeof(path), "%s%s.tga", path, fileName);
	
	File file = OpenFile(path, "wb");
	if (file == null)
	{
		LogError("[rt] Failed to create file to write to: \"%s\".", path);
		return;
	}
	WriteFile(file, tga, sizeof(tga), 1);
	WriteFile(file, g_iPicture, g_cvRaytracePicWidth.IntValue * g_cvRaytracePicHeight.IntValue * 3, 1);
	
	PrintToServer("File done \"%s\"", path);
	delete file;
}